package model;

public class Seat {
	private int time;
	private char row;
	private int col;
	private int price;

	public Seat(Integer time, char row, int col, int price) {
		this.time = time;
		this.row = row;
		this.col = col;
		this.price = price;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		Seat otherSeat = ((Seat)other);
		if (otherSeat.time == this.time
		  && otherSeat.row == this.row
		  && otherSeat.col == this.col
		  && otherSeat.price == this.price){
			return true;
		}
		return false;
	}
}
